import urllib.parse
import re
from repository.sql_repository import Sql_Repository

sql_repository = Sql_Repository.get_instance()

def generate_url_to_sale(steam_app_id:int,name:str,store_id:int):
    if store_id == "1":
        return f"https://store.steampowered.com/app/{steam_app_id}"
    elif store_id == "2":
        name = name.replace(':','')
        name = name.replace(' ','+')
        return f"https://www.gamersgate.com/games/?query={name}"
    elif store_id == "3":
        name = name.replace(' ','-')
        name = re.sub(r'\([^)]*\)', '', name)
        name = urllib.parse.quote(name)
        return f"https://www.greenmangaming.com/search?query={name}"
    elif store_id == "7":
        name = urllib.parse.quote(name)
        return f"https://www.gog.com/pl/games?query={name}"
    elif store_id == "11":
        name = urllib.parse.quote(name)
        return f"https://www.humblebundle.com/store/search?sort=bestselling&search={name}&page=1"
    elif store_id == "21":
        name = name.replace(' ','+')
        return f"https://www.wingamestore.com/search/?SearchWord={name}"
    elif store_id == "23":
        name = name.upper()
        name = name.replace(' ','+')
        return f"https://www.gamebillet.com/search?q={name}"
    elif store_id == "24":
        name = urllib.parse.quote(name)
        return f"https://www.voidu.com/pl/search/{name}"
    elif store_id == "25":
        name = name.replace(':','')
        name = urllib.parse.quote(name)
        return f"https://store.epicgames.com/pl/browse?q={name.lower()}"
    #TODO FIX url generator
    elif store_id == "27":
        name = name.replace(' ','+')
        return f"https://us.gamesplanet.com/search?query={name}"
    elif store_id == "28":
        name = name.replace(' ','+')
        return f"https://www.gamesload.eu/results.html?search={name}"
    elif store_id == "29":
        name = name.replace(' ','+')
        return f"https://2game.com/pl/catalogsearch/result/?q={name}"
    elif store_id == "30":
        name = urllib.parse.quote(name)
        return f"https://www.indiegala.com/search/{name}/store-games"
    elif store_id == "31":
        name = name.replace(' ','-')
        return f"https://eu.shop.battle.net/pl-pl/product/{name}"
    elif store_id == "33":
        name = name.replace(':','')
        name = name.replace(' ','+')
        return f"https://www.dlgamer.com/us/search?keywords={name}"
    elif store_id == "34":
        name = name.replace(' ','+')
        return f"https://www.noctre.com/search?s={name}"
    elif store_id == "35":
        name = name.replace(' ','+')
        return f"https://www.dreamgame.com/pl/search?Box=true&q={name}"
    else:
        shop = sql_repository.get_shop_by_id(store_id)
        return f"https://www.google.com/search?q={shop.storeName}"