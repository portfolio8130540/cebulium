import atexit
import time
from apscheduler.schedulers.background import BackgroundScheduler
from services.cheap_shark import get_all_shops, get_active_sales
from repository.sql_repository import Sql_Repository

class Sheduler():
    __instance = None
    __scheduler = None
    __sql_repository = Sql_Repository.get_instance()
    
    @classmethod
    def get_instance(cls):
        if cls.__instance == None:
            cls.__instance = cls.__new__(cls)
            cls.__sql_repository.delete_all_deals()
            cls.get_all_shops_job(cls.__instance)
            cls.get_active_sales_job(cls.__instance)
            cls.__instance.__scheduler = BackgroundScheduler()
            #cls.__instance.start_jobs()
        return cls.__instance

    def __init__(self):
        raise RuntimeError("This is a Singleton, invoke get_instance() insted.")
    
    def get_all_shops_job(self):
        print("Update shops list")
        get_all_shops()
    
    def get_active_sales_job(self):
        print("Update sales list ")
        get_active_sales()

    # def start_jobs(self):
    #     self.__scheduler.add_job(func=self.get_all_shops_job, trigger="interval", seconds=86400)
    #     self.__scheduler.add_job(func=self.get_active_sales_job, trigger="interval", seconds=60)
    #     self.__scheduler.start()
    #     atexit.register(lambda: self.__scheduler.shutdown())