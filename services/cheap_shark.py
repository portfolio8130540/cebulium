import requests
from configuration.configuration_manager import Configuration_Manager
from repository.sql_repository import Sql_Repository
import services.store_service as ss

configuration = Configuration_Manager.get_instance()
sql_repository = Sql_Repository.get_instance()

counter_req:int = 0

def get_headers():
    return {
	"X-RapidAPI-Key": configuration.get_config_by_key('cheapshark.key'),
	"X-RapidAPI-Host": configuration.get_config_by_key('cheapshark.host')
    }



def get_all_shops():
    url = configuration.get_config_by_key('cheapshark.urls.store')
    headers = get_headers()
    response = requests.get(url,headers=headers)
    for shop in response.json():
        shop_tuple = (shop.get('storeID'),shop.get('storeName'),shop.get('isActive'),shop.get('images').get('logo'))
        sql_repository.insert_shop(shop=shop_tuple)

def get_images(image_url:str):
    url = configuration.get_config_by_key('cheapshark.urls.store')+image_url
    headers = get_headers()
    return requests.get(url,headers=headers)

def get_active_sales():
    global counter_req
    url = configuration.get_config_by_key('cheapshark.urls.deals')
    headers = get_headers()
    response = requests.get(url,headers=headers)
    if response.status_code == 200:
        if counter_req == 60:
            counter_req = 0
            print("Clear database deals")
            sql_repository.delete_all_deals()
        counter_req = counter_req + 1 
        for deal in response.json():
            saving = round(float(deal.get('normalPrice'))-float(deal.get('salePrice')),2)
            sale_url = ss.generate_url_to_sale(deal.get('steamAppID'),deal.get('title'),deal.get('storeID'))
            deal_tuple = (deal.get('dealID')
                      ,deal.get('title')
                      ,deal.get('storeID')
                      ,deal.get('gameID')
                      ,deal.get('salePrice')
                      ,deal.get('normalPrice')
                      ,sale_url
                      ,deal.get('dealRating')
                      ,deal.get('thumb')
                      ,saving)
            sql_repository.insert_deal(deal=deal_tuple)
    else:
        print("Can't update sales list")