from repository.sql_repository import Sql_Repository
from services.sheduler import Sheduler
from entity.deal import Deal
from entity.shop import Shop
import math

sql_repository = Sql_Repository.get_instance()
Sheduler.get_instance()

def get_deals(query:str,sort:str,page:int):
    if query == '' or query == None:
        deals = sql_repository.get_deals()
    else:
        deals = sql_repository.get_deals_by_title(query)
    if sort == 'Title':
        deals = sorted(deals,key=lambda deal:deal.title)
    elif sort == 'Store':
        deals = sorted(deals,key=lambda deal:deal.storeID)
    elif sort == 'Saving':
        deals = sorted(deals,key=lambda deal:deal.savings,reverse=1)
    elif sort == "Price":
        deals = sorted(deals,key=lambda deal:deal.salePrice)
    deals_dto:[Deal,Shop] = []
    for deal in deals:
        store =  sql_repository.get_shop_by_id(deal.storeID)
        if store:
            deals_dto.append([deal,store])
    page_from = 10*(int(page) - 1)
    page_to = 10 * int(page)
    return deals_dto[page_from:page_to]

def count_deals_pages(query:str):
    if query == '' or query == None:
        pages = sql_repository.count_deals()
    else:
        pages = sql_repository.count_deals_by_title(query)
    pages = math.ceil(pages/10)+1
    return pages