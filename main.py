from flask import Flask, render_template,request,make_response,session
from flask_session import Session
from configuration.configuration_manager import Configuration_Manager
from services.mediator import get_deals,count_deals_pages
from services.cheap_shark import get_images
import os


app = Flask(__name__)
port = int(os.environ.get("PORT", 5000))
SESSION_TYPE = "redis"
PERMANENT_SESSION_LIFETIME = 1800
app.config.update(SECRET_KEY=os.urandom(24))

configuration = Configuration_Manager.get_instance()


@app.route('/',methods=['GET'])
def home():
    #GET SORT TYPE
    if request.args.get('sort') == None:
        if request.cookies.get('sort') == None:
            sort = 'Title'
        else:
            sort = request.cookies.get('sort')
    else:
        page = 1
        session['page'] = page
        sort = request.args.get('sort')

    #GET CURRENT PAGE
    if request.args.get('page') != None:
        page = request.args.get('page')
    else:
        if 'page' in session:
            page = session['page']
        else:
            page = 1
    page = int(page)
   
    session['page'] = page
    
    #GET SEARCH QUERY
    query = request.args.get('query')
    pages = count_deals_pages(query)
    if page < 1:
        page = 1 
    if page > pages-1:
        page = pages-1
    deals = get_deals(query,sort,page)
    if query == None:
        query = ''
    first_page = 1 if int(page) < 4 else int(page)-2 
    response = make_response(render_template("index.html",len=len(deals),Deals = deals,Search = query,Sort=sort,Pages=pages,Page=page,First_page=first_page))
    response.set_cookie('sort',sort)
    return response

@app.context_processor
def utility_processor():
    def get_image(image:str):
        return get_images(image)

    return dict(get_image=get_image)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)
