import json

class Shop():
    id:int
    storeID:int
    storeName:str
    isActive:int
    images:str

    def __init__(self,id:int,storeID:int,storeName:str,isActive:int,images:str):
        self.id=id
        self.storeID=storeID
        self.storeName=storeName
        self.isActive=isActive
        self.images=images

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)

    def to_tuple(self):
        return (self.storeID,self.storeName,self.isActive,self.images)
