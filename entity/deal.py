class Deal:
    def __init__(self, id, dealID, title, storeID, gameID, salePrice, normalPrice, steamAppID, dealRating, thumb,savings):
        self.id = id
        self.dealID = dealID
        self.title = title
        self.storeID = storeID
        self.gameID = gameID
        self.salePrice = salePrice
        self.normalPrice = normalPrice
        self.steamAppID = steamAppID
        self.dealRating = dealRating
        self.thumb = thumb
        self.savings = savings