import sqlite3
from entity.shop import Shop
from entity.deal import Deal

class Sql_Repository():
    __instance = None
    __conn:sqlite3.Connection
    __cur:sqlite3.Cursor

    @classmethod
    def get_instance(cls):
        if cls.__instance == None:
            cls.__instance = cls.__new__(cls)
            cls.__init_database(cls.__instance)
        return cls.__instance

    def __init__(self):
        raise RuntimeError("This is a Singleton, invoke get_instance() insted.")

    def __open_connection(self):
        self.__conn = sqlite3.connect('cheapgames.db')
        self.__cur = self.__conn.cursor()

    def __close_connection(self):
        self.__cur.close()
        self.__conn.close()

    def __init_database(self):
        self.__open_connection()
        self.__cur.execute("CREATE TABLE IF NOT EXISTS shops(id integer primary key autoincrement,storeID integer,storeName TEXT,isActive int,images TEXT,UNIQUE(storeID))")
        self.__conn.commit()
        self.__cur.execute("CREATE TABLE IF NOT EXISTS deals(id integer primary key autoincrement,dealID integer,title TEXT,storeID int,gameID int,salePrice REAL,normalPrice REAL,steamAppID TEXT,dealRating REAL,thumb TEXT,savings REAL,UNIQUE(dealID))")
        self.__conn.commit()
        self.__close_connection()

    def insert_shop(self,shop):
        self.__open_connection()
        self.__cur.execute(f"Insert OR IGNORE into shops(storeID,storeName,isActive,images) values(?,?,?,?)",shop)
        self.__conn.commit()
        self.__close_connection()
    
    def get_shops(self):
        self.__open_connection()
        self.__cur.execute("SELECT * FROM shops")
        shop_list:[Shop] = []
        for id,storeID,storeName,isActive,images in self.__cur:
           shop_list.append(Shop(id,storeID,storeName,isActive,images))
        self.__close_connection()
        return shop_list
    
    def get_shop_by_id(self,id:int):
        self.__open_connection()
        storeID = (id,)
        self.__cur.execute("SELECT * FROM shops where storeID = ?",storeID)
        for id,storeID,storeName,isActive,images in self.__cur:
           shop = Shop(id,storeID,storeName,isActive,images)
        self.__close_connection()
        return shop
    
    def insert_deal(self,deal):
        self.__open_connection()
        self.__cur.execute(f"Insert OR IGNORE into deals(dealID,title,storeID,gameID,salePrice,normalPrice,steamAppID,dealRating,thumb,savings) values(?,?,?,?,?,?,?,?,?,?)",deal)
        self.__conn.commit()
        self.__close_connection()
    
    def get_deals(self):
        self.__open_connection()
        self.__cur.execute("SELECT * FROM deals")
        deal_list:[Deal] = []
        for id,dealID,title,storeID,gameID,salePrice,normalPrice,steamAppID,dealRating,thumb,savings in self.__cur:
           deal_list.append(Deal(id,dealID,title,storeID,gameID,salePrice,normalPrice,steamAppID,dealRating,thumb,savings))
        self.__close_connection()
        return deal_list
    
    def get_deals_by_title(self,title):
        self.__open_connection()
        title = '%'+title+'%'
        self.__cur.execute("SELECT * FROM deals where title LIKE ?",(title,))
        deal_list:[Deal] = []
        for id,dealID,title,storeID,gameID,salePrice,normalPrice,steamAppID,dealRating,thumb,savings in self.__cur:
           deal_list.append(Deal(id,dealID,title,storeID,gameID,salePrice,normalPrice,steamAppID,dealRating,thumb,savings))
        self.__close_connection()
        return deal_list
    
    def count_deals(self):
        self.__open_connection()
        self.__cur.execute("SELECT COUNT(title) as rows FROM deals")
        for rows in self.__cur:
            row = rows
        self.__close_connection()
        return row[0]
    
    def count_deals_by_title(self,title):
        self.__open_connection()
        title = '%'+title+'%'
        self.__cur.execute("SELECT COUNT(title) as rows FROM deals where title LIKE ?",(title,))
        for rows in self.__cur:
            row = rows
        self.__close_connection()
        return row[0]
    
    def delete_all_deals(self):
        self.__open_connection()
        self.__cur.execute("DELETE FROM deals")
        self.__conn.commit()
        self.__close_connection()

